/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   materials.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 20:25:20 by tmielcza          #+#    #+#             */
/*   Updated: 2014/03/25 19:15:11 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"
#include "scene.h"

void			material_init(t_obj *obj, t_mater mat)
{
	obj->mat.diff.r = mat.diff.r * ((double)obj->color.r / 255);
	obj->mat.diff.g = mat.diff.g * ((double)obj->color.g / 255);
	obj->mat.diff.b = mat.diff.b * ((double)obj->color.b / 255);
	obj->mat.spec.r = mat.spec.r * ((double)obj->color.r / 255);
	obj->mat.spec.g = mat.spec.g * ((double)obj->color.g / 255);
	obj->mat.spec.b = mat.spec.b * ((double)obj->color.b / 255);
	obj->mat.shin = mat.shin;
	obj->mat.refl = mat.refl;
	obj->mat.sblur = mat.sblur;
}

/*
**	A material is define as : Name, Diff coefs, Spec coefs, Spec pow, Reflect
**	Name = The name of the material
**	Diff coefs = Coefficients for R, G and B to compute diffuse light
**	Spec coefs = Coefficients for R, G and B to compute specular light
**	Spec pow = Exponent to compute specular light
**	Reflect = Coefficient of reflectance
*/
t_mater			materials(int i)
{
	static t_mater	mat[] =
		{
			{"mat1", {1, 1, 1}, {1, 1, 1}, 100, 0.3, 0.01},
			{"mat2", {1, 1, 1}, {1, 1, 1}, 57, 0.7, 7},
			{"mat3", {1, 1, 1}, {1, 1, 1}, 70, 0.3, 7}
		};
	return (mat[i]);
}
