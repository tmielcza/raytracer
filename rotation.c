/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 16:19:55 by tmielcza          #+#    #+#             */
/*   Updated: 2014/03/13 14:53:12 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <math.h>
#include "rtv1.h"

void			rot_apply(t_vect *rot, double **mat)
{
	t_vect		vec;

	vec = *rot;
	rot->x = vec.x * mat[0][0] + vec.y * mat[1][0] + vec.z * mat[2][0];
	rot->y = vec.x * mat[0][1] + vec.y * mat[1][1] + vec.z * mat[2][1];
	rot->z = vec.x * mat[0][2] + vec.y * mat[1][2] + vec.z * mat[2][2];
}

static double	**rot_init_matrix(void)
{
	double	**matrix;
	int	i;

	i = 0;
	matrix = (double **)malloc(3 * sizeof(double *));
	while (i < 3)
	{
		matrix[i] = (double *)malloc(3 * sizeof(double));
		i++;
	}
	return (matrix);
}

/*
**	Initiates a vertical matrix of the form
**		  (  1    0    0  )
**	M(ß)= (  0  cosß -sinß)
**		  (  0  sinß  cosß)
*/
double			**rot_init_x_matrix(double angle)
{
	int		i;
	double		**matrix;
	double		mat[3][3] =
		{
			{1, 0, 0},
			{0, cos(angle), -sin(angle)},
			{0, sin(angle), cos(angle)}
		};

	i = 0;
	matrix = rot_init_matrix();
	while (i < 9)
	{
		matrix[i / 3][i % 3] = mat[i / 3][i % 3];
		i++;
	}
	return (matrix);
}

/*
**	Initiates a horizontal matrix of the form
**		  ( cosß  0   sinß)
**	M(ß)= (  0    1    0  )
**		  (-sinß  0   cosß)
*/
double			**rot_init_y_matrix(double angle)
{
	int		i;
	double		**matrix;
	double		mat[3][3] =
		{
		{cos(angle), 0, sin(angle)},
		{0, 1, 0},
		{-sin(angle), 0, cos(angle)}
		};

	i = 0;
	matrix = rot_init_matrix();
	while (i < 9)
	{
		matrix[i / 3][i % 3] = mat[i / 3][i % 3];
		i++;
	}
	return (matrix);
}

double			**rot_init_z_matrix(double angle)
{
	int		i;
	double		**matrix;
	double		mat[3][3] =
		{
			{cos(angle), -sin(angle), 0},
			{sin(angle), cos(angle), 0},
			{0, 0, 1}
		};

	i = 0;
	matrix = rot_init_matrix();
	while (i < 9)
	{
		matrix[i / 3][i % 3] = mat[i / 3][i % 3];
		i++;
	}
	return (matrix);
}
