/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   crado2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
<<<<<<< HEAD
/*   Created: 2014/01/07 15:37:56 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/22 16:17:31 by tmielcza         ###   ########.fr       */
=======
/*   Created: 2014/03/23 20:28:58 by tmielcza          #+#    #+#             */
/*   Updated: 2014/03/23 20:29:14 by tmielcza         ###   ########.fr       */
>>>>>>> ec1aa0e08a0d7669b006be5bebc721cf980c772d
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "rtv1.h"

void		ft_get_cyl(t_cyl *s, t_obj *o)
{
	pos_get(&o->or, 0, 100, 0);
	s->rad = 0.2;
	o->color.r = 0;
	o->color.g = 255;
	o->color.b = 0;
	material_init(o, materials(_mat1));
	o->type = CYLINDER;
	o->rot.y = rot_init_y_matrix(TORAD(0));
	o->rot.x = rot_init_x_matrix(TORAD(0));
	o->invrot.y = rot_init_y_matrix(TORAD(0));
	o->invrot.x = rot_init_x_matrix(TORAD(0));
	o->rot.z = rot_init_z_matrix(TORAD(0));
	o->invrot.z = rot_init_z_matrix(TORAD(0));
}

void		ft_get_cone(t_cone *s, t_obj *o)
{
	pos_get(&o->or, -5, 2, 3);
	s->angle = 0.2;
	s->tan_a = square(tan(s->angle));
	o->color.r = 140;
	o->color.g = 255;
	o->color.b = 100;
	material_init(o, materials(_mat1));
	o->type = CONE;
	o->rot.y = rot_init_y_matrix(TORAD(0));
	o->rot.x = rot_init_x_matrix(TORAD(90));
	o->rot.z = rot_init_z_matrix(TORAD(120));
	o->invrot.y = rot_init_y_matrix(TORAD(0));
	o->invrot.x = rot_init_x_matrix(TORAD(-90));
	o->invrot.z = rot_init_z_matrix(TORAD(-120));
}
