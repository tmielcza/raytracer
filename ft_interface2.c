/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_interface2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cmartin <cmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/26 14:54:46 by cmartin           #+#    #+#             */
/*   Updated: 2014/03/27 15:05:52 by cmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"
#include <mlx.h>
#include <stdio.h>


void	ft_fill_pixel_img1(int x, int y, t_env *env)
{
	int             *tmp;
	t_color        color;
	unsigned int    color2;

	tmp = (int *)env->img.pxl;
	color.r = 153;
	color.g = 0;
	color.b = 0;
	color.color = COL(color.r, color.g, color.b);
	color2 = mlx_get_color_value(env->mlx, color.color);
	tmp[y * (env->img.ln / 4) + x] = color2;
}

int	ft_fill_img1(t_env *e)
{
	int		x;
	int		y;

	x = 0;
	y = 0;
	while ((x + 1) < WIDTH)
	{
		mlx_pixel_put(e->mlx, e->win, x + 50, y + 30, 0xFF0000);
		x++;
	}
	while ((y + 1) < HEIGHT)
	{
		mlx_pixel_put(e->mlx, e->win, x + 50, y + 30, 0xFF0000);
		y++;
	}
	while (x > 0)
	{
		mlx_pixel_put(e->mlx, e->win, x + 50, y + 30, 0xFF0000);
		x--;
	}
	while (y > 0)
	{
		mlx_pixel_put(e->mlx, e->win, x + 50, y + 30, 0xFF0000);
		y--;
	}
	return (-1);
}
