    /* ************************************************************************** */
    /*                                                                            */
    /*                 RAYTRACER                              :::      ::::::::   */
    /*                                                      :+:      :+:    :+:   */
    /*                                                    +:+ +:+         +:+     */
    /*    By: caupetit <caupetit@student.42.fr>          +#+  +:+       #+        */
    /*    By: tmielcza <tmielcza@student.42.fr>       +#+#+#+#+#+   +#+           */
    /*    By: cmartin   <cmartin@student.42.fr>            #+#    #+#             */
    /*    By: shedelin <shedelin@student.42.fr>           ###   ########.fr       */
    /*                                                                            */
    /* ************************************************************************** */

****

This program displays 3D scenes using raytracing method in a simple graphic interface.

What's implemented :

* Some primitives : Sphere, Cylinder, Cone, Plan, Ellipsoid
* Materials for Phong lighting (Diffuse, Specular, Ambient)
* Shadows
* Smooth reflection (using antialiasing)

### Setup ###

Only works on mac OSX. It uses libft, a simple implementation of some standard c functions. See [libft/](https://bitbucket.org/procrade/raytracer/src/c8fac09f7a22c649345937a4079a80801d970d9c/libft/?at=master)

Needed MinilibiX graphic library: https://cdn.42.fr/elearning/INFOG-1-001/mlx-20140106.tgz

Scenes are described in files. See [scene.h](https://bitbucket.org/procrade/raytracer/src/c8fac09f7a22c649345937a4079a80801d970d9c/scene.h?at=master) to know how to create such a file.

Exemple of 3D file [here](https://bitbucket.org/procrade/raytracer/src/c8fac09f7a22c649345937a4079a80801d970d9c/map.sc?at=master).

****

Use:

- make

- ./RT

### Screenshots ###

- Basic scene showing primitives and features.

![Screen Shot Raytracer - Exemple scene.png](https://bitbucket.org/repo/M7gAaA/images/2374751987-Screen%20Shot%20Raytracer%20-%20Exemple%20scene.png)

- 10 Materials we configured. They can be set to every primitives.

![Screen Shot Raytracer - Materials.png](https://bitbucket.org/repo/M7gAaA/images/3333959383-Screen%20Shot%20Raytracer%20-%20Materials.png)

- We can see multiple levels of reflection, various primitives and materials, and colored spots.

![Screen Shot Raytracer - Big scene.png](https://bitbucket.org/repo/M7gAaA/images/3209920556-Screen%20Shot%20Raytracer%20-%20Big%20scene.png)

- Exemple of basic scene showing shadows, shining and reflection.

![Screen Shot Raytracer - shadows.png](https://bitbucket.org/repo/M7gAaA/images/2188153625-Screen%20Shot%20Raytracer%20-%20shadows.png)

- pausing during calculation.

![Screen Shot Raytracer - pause.png](https://bitbucket.org/repo/M7gAaA/images/137591889-Screen%20Shot%20Raytracer%20-%20pause.png)

- fail

![Screen Shot Raytracer - fail 1.png](https://bitbucket.org/repo/M7gAaA/images/3967851274-Screen%20Shot%20Raytracer%20-%20fail%201.png)