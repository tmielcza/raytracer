# Tout plein d'objets

  	   camera
position	0 10 -50
look		0 0 0
direction	0 1 0



# spots
  spot
position	10 100 -40
color		255 255 255
  spot
position	10 10 -40
color		255 255 255


# objets
  object
type
	plane
	direction	0 -1 0
position	0 0 0
color		135 89 26
rotation_xyz	0 0 0
material
	mat2

		  object
type
	cylinder
	size		20
position		0 0 0
color			58 157 35
rotation_xyz	10 0 10
material
	mat2

		  object
type
	cylinder
	size		20
position		12 0 30
color			58 157 35
rotation_xyz	355 12 5
material
	mat2
		  object
type
	cylinder
	size		20
position		-1 0 -3
color			58 157 35
rotation_xyz	5 5 358
material
	mat2

		  object
type
	cylinder
	size		20
position		3 2 -10
color			58 157 35
rotation_xyz	350 15 2
material
	mat2

		  object
type
	cylinder
	size		20
position		-6 -3 -13
color			58 157 35
rotation_xyz	1 353 6
material
	mat2
