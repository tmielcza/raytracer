/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   crado.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
<<<<<<< HEAD
/*   Created: 2014/01/07 15:37:56 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/22 16:18:55 by tmielcza         ###   ########.fr       */
=======
/*   Created: 2014/03/23 20:27:14 by tmielcza          #+#    #+#             */
/*   Updated: 2014/03/23 20:28:37 by tmielcza         ###   ########.fr       */
>>>>>>> ec1aa0e08a0d7669b006be5bebc721cf980c772d
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void		ft_get_sphr1(t_sphr *s, t_obj *o)
{
	pos_get(&o->or, 0, 0, 0);
	s->rad = 1;
	o->mat = materials(_mat2);
	o->color.r = 255;
	o->color.g = 255;
	o->color.b = 255;
	o->mat = materials(_mat2);
	o->type = SPHERE;
	o->rot.y = rot_init_y_matrix(TORAD(50));
	o->rot.x = rot_init_x_matrix(TORAD(-10));
	o->invrot.y = rot_init_y_matrix(TORAD(-50));
	o->invrot.x = rot_init_x_matrix(TORAD(10));
	o->rot.z = rot_init_z_matrix(0);
	o->invrot.z = rot_init_z_matrix(0);
}

void		ft_get_ellips(t_ellips *e, t_obj *o)
{
	pos_get(&o->or, 4, -2, 0);
	e->size.x = 5;
	e->size.y = 3;
	e->size.z = 1;
	o->mat = materials(_mat2);
	o->color.r = 0;
	o->color.g = 0;
	o->color.b = 255;
	o->mat = materials(_mat2);
	o->type = ELLIPSOID;
	o->rot.y = rot_init_y_matrix(TORAD(0));
	o->rot.x = rot_init_x_matrix(TORAD(180));
	o->invrot.y = rot_init_y_matrix(TORAD(0));
	o->invrot.x = rot_init_x_matrix(TORAD(-180));
	o->rot.z = rot_init_z_matrix(0);
	o->invrot.z = rot_init_z_matrix(0);
}

void		ft_get_sphr3(t_sphr *s, t_obj *o)
{
	pos_get(&o->or, 0, 4, 0);
	s->rad = 0.5;
	o->mat = materials(_mat3);
	o->color.r = 255;
	o->color.g = 255;
	o->color.b = 0;
	o->mat = materials(_mat3);
	o->type = SPHERE;
	o->rot.y = rot_init_y_matrix(0);
	o->rot.x = rot_init_x_matrix(0);
	o->invrot.y = rot_init_y_matrix(0);
	o->invrot.x = rot_init_x_matrix(0);
	o->rot.z = rot_init_z_matrix(0);
	o->invrot.z = rot_init_z_matrix(0);
}

void		ft_get_sphr4(t_sphr *s, t_obj *o)
{
	pos_get(&o->or, 0, 0, 6);
	s->rad = 0.5;
	o->color.r = 225;
	o->color.g = 225;
	o->color.b = 0;
	material_init(o, materials(_mat2));
	o->type = SPHERE;
	o->rot.y = rot_init_y_matrix(TORAD(60));
	o->rot.x = rot_init_x_matrix(TORAD(-10));
	o->invrot.y = rot_init_y_matrix(TORAD(-60));
	o->invrot.x = rot_init_x_matrix(TORAD(10));
	o->rot.z = rot_init_z_matrix(120);
	o->invrot.z = rot_init_z_matrix(-120);
}

void		ft_get_plane(t_pln *s, t_obj *o)
{
	pos_get(&o->or, 0, -3, 0);
	vect_get(&s->dir, 0, -1, 0);
	vect_normalise(&s->dir, 0);
	o->mat = materials(_mat1);
	o->color.r = 255;
	o->color.g = 0;
	o->color.b = 0;
	o->mat = materials(_mat1);
	o->type = PLANE;
	o->rot.y = rot_init_y_matrix(TORAD(0));
	o->rot.x = rot_init_x_matrix(0);
	o->invrot.y = rot_init_y_matrix(TORAD(0));
	o->invrot.x = rot_init_x_matrix(0);
	o->rot.z = rot_init_z_matrix(0);
	o->invrot.z = rot_init_z_matrix(0);
}
